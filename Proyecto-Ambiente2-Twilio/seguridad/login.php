<?php
$title = 'Login';
require_once '../shared/header.php';
require_once '../shared/db.php';
require_once '../shared/sessions.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
  $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
  $user = $usuario_model->login($email, $password);
  if ($user != null) {
    $_SESSION['usuario_id'] = $user['id'];
    return header('Location: /home');
  } else {
    echo "<h3>Usuario o contraseña inválido!</h3>";
  }
}
?>

<div class="container">

  <div class="row" style="display: flex;flex-direction: row;justify-content: center;">
    <img src="/assets/imgs/twilio-logo.png" alt="" class="text-center"><br> 
    <div class="col-md-6">
      <form method="POST">
        <div class="form-group">
          <label for="email"><b>Email address</b></label>
          <input type="email" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter email" name="email">
        </div>
        <div class="form-group">
          <label for="password"><b>Contraseña</b></label>
          <input type="password" class="form-control" id="password" placeholder="Password" name="password">
        </div>
        <div style="display:flex;justify-content: center;">
          <input class="btn btn-primary" type="submit" value="Login!">
          <a class="btn btn-default" href="/seguridad/signup.php">Signup</a>
        </div>
        
      </form>
    </div>
  </div>
</div>
