<?php
$title = 'Twilio';
require_once '../shared/header.php';
require_once '../shared/sessions.php';
require_once '../shared/db.php';
if (!isset($_SESSION['usuario_id']) || empty($_SESSION['usuario_id'])) {
	return header('Location: /seguridad/login.php');
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>


	<div  style="display: flex;justify-content: center; " >
		<div class="card text-white bg-primary mb-2" style="max-width: 22rem;margin-top:20px">
			<div class="card-header text-center" ><h5 class="card-title">Servicio</h5></div>
			<div class="card-body">
				
				<p class="card-text text-center"><?php 
				if (isset($_SESSION['sid'])) {
					echo "".$_SESSION['sid'];
				}else{
					print("no hay servicio");
				}
				?></p>
			</div>
		</div><br>
	</div>
</body>
</html>
