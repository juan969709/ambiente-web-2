<?php
$title = 'Twilio';
require_once '../shared/header.php';
require_once '../shared/sessions.php';
require_once '../shared/db.php';
require_once '../vendor/autoload.php';


if (!isset($_SESSION['usuario_id']) || empty($_SESSION['usuario_id'])) {
  return header('Location: /seguridad/login.php');
}
use Twilio\Rest\Client;

// Find your Account Sid and Auth Token at twilio.com/console
$sid    = "AC8ef2cdda2dba644616b3a63a56fcafc8";
$token  = "b5c0e4f89efc911ba9f0479d09e4c27e";
$twilio = new Client($sid, $token);


$service = $twilio->chat->v2->services
->create("FRIENDLY_NAME");
if (!isset($_SESSION['sid'])) {

  $service =$twilio->chat->v2->services->create("Nombre_Servicio");

  $_SESSION['sid'] =$service->sid;

}else{

  $service=$twilio->chat->v2->services($_SESSION['sid'])
  ->fetch();
}


?>

<div class="text-center" style="margin-top:100px">
	<img src="/assets/imgs/twilio-logo.png" alt=""><br>	

 <div class="dropdown">
  <form action="../members/add-member.php" method="post"> 
    <label for="idCanal"><b>Seleccionar Canal</b></label><br>
    <select style="width:600px" id="idCanal" name="idCanal" class="btn btn-secondary dropdown-toggle" required>
      <?php
      require_once '../channels-twilio-crud/lista-canales.php';
      if ($channels) {
        foreach ($channels as $record) {
          echo "<option value='".$record->sid."'>Canal: ".$record->uniqueName."</option>";
        }
      }else{
       echo "<option>No hay canales para mostrar</option>";
     }
     
     ?>
   </select><br><br>

   <div class="form-group text-center" style="display: flex;justify-content: center;">
    <input type="text" style="width:600px" class="form-control" placeholder="userName" name="userName" required>
  </div>
  
  <button class="btn btn-success" style="width:600px">Unirse al Chat!</button>
  
</form>

</div>

</div>