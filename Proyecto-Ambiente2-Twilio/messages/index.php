<?php
$title = 'Twilio';
require_once '../shared/header.php';
require_once '../shared/sessions.php';
require_once '../shared/db.php';
require_once '../vendor/autoload.php';
require_once '../shared/sessions.php';


$channel = filter_input(INPUT_GET, 'can', FILTER_SANITIZE_STRING);
$member = filter_input(INPUT_GET, 'member', FILTER_SANITIZE_STRING);
$userName = filter_input(INPUT_GET, 'userName', FILTER_SANITIZE_STRING);

if (!isset($_SESSION['usuario_id']) || empty($_SESSION['usuario_id'])) {
    return header('Location: /seguridad/login.php');
}



?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body class="container">
	<h1 class="text-center">Mensajes</h1>

<div class="text-center" style="display:flex;justify-content: center;flex-direction:column;align-items:center;background-color:#000000;">
<?php 

require_once '../messages/list-messages.php';

 ?>
 <br><br>
	<form action="send-message.php?channel=<?=$channel ?? ''?>&member=<?=$member ?? ''?>" method="post">
<div style="display: flex;flex-direction: row;">
	
              <select name="userName" class="form-control mr-2" value="" style="width: 145px">
                <option value="<?=$userName ?? ''?>"><?=$userName ?? ''?></option>
              </select>
    <input type="text" class="mr-2" placeholder="Mensaje" name="mensaje" style="width:400px;" required>
    	<button class="btn btn-success mr-2" type="submit">SEND  MESSAGE</button>
</div>
           

</form>
<br><br>
</div><br><br><br><br>

</body>
</html>
