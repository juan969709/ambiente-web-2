<?php

// Update the path below to your autoload.php,
// see https://getcomposer.org/doc/01-basic-usage.md
require_once '../vendor/autoload.php';
require_once '../shared/sessions.php';

use Twilio\Rest\Client;

// Find your Account Sid and Auth Token at twilio.com/console
$sid    = "AC8ef2cdda2dba644616b3a63a56fcafc8";
$token  = "b5c0e4f89efc911ba9f0479d09e4c27e";
$twilio = new Client($sid, $token);


$channel = filter_input(INPUT_GET, 'channel', FILTER_SANITIZE_STRING);
$mensaje = filter_input(INPUT_POST, 'mensaje', FILTER_SANITIZE_STRING);
$member = filter_input(INPUT_GET, 'member', FILTER_SANITIZE_STRING);
$userName = filter_input(INPUT_POST, 'userName', FILTER_SANITIZE_STRING);

$dateCreated=date(DATE_ISO8601);
$message = $twilio->chat->v2->services($_SESSION['sid'])
                            ->channels($channel)
                            ->messages
                            ->create(array("body" => $mensaje,"from" => $userName,"dateCreated" => $dateCreated));

 return header('Location: /messages/index.php?can='.$channel.'&member='.$member.'&userName='.$userName.'&member='.$member.'');