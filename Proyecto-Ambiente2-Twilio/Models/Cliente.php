<?php
namespace Models {
    class Cliente
    {
        private $connection;
        public function __construct($connection)
        {
            $this->connection = $connection;
        }

        public function find($id)
        {
            return $this->connection->runQuery('SELECT * FROM clientes WHERE id = $1', [$id])[0];
        }
        public function find_by_cedula($cedula)
        {
            return $this->connection->runQuery('SELECT * FROM clientes WHERE cedula = $1', [$cedula]);
        }

        public function select()
        {
            return $this->connection->runQuery('SELECT * FROM clientes ORDER BY id');
        }

        public function insert($cedula, $nombre, $apellidos, $telefono)
        {
            $this->connection->runStatement('INSERT INTO clientes(cedula,nombre,apellidos,telefono) VALUES ($1,$2,$3,$4)', [$cedula, $nombre, $apellidos, $telefono]);
        }

        public function update($id, $cedula, $nombre, $apellidos, $telefono)
        {
            $this->connection->runStatement('UPDATE clientes SET  cedula = $2, nombre=$3, apellidos=$4, telefono=$5 WHERE id = $1', [$id, $cedula, $nombre, $apellidos, $telefono]);
        }

        public function delete($id)
        {
            $this->connection->runStatement('DELETE FROM clientes WHERE id = $1', [$id]);
        }
    }
}
