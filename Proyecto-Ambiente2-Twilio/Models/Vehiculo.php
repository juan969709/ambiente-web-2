<?php
namespace Models {
    class Vehiculo
    {
        private $connection;
        public function __construct($connection)
        {
            $this->connection = $connection;
        }

        public function find($id)
        {
            return $this->connection->runQuery('SELECT * FROM vehiculos WHERE id = $1', [$id])[0];
        }
          public function find_by_placa($placa)
        {
            return $this->connection->runQuery('SELECT * FROM vehiculos WHERE placa = $1', [$placa])[0];
        }
              public function find_by_pla($placa)
        {
            return $this->connection->runQuery('SELECT * FROM vehiculos WHERE placa = $1', [$placa]);
        }

        public function select()
        {
            return $this->connection->runQuery('SELECT * FROM vehiculos ORDER BY id');
        }

        public function insert($placa,$marca, $modelo,$kilometraje,$notas)
        {
            $this->connection->runStatement('INSERT INTO vehiculos(placa,marca, modelo,kilometraje,notas) VALUES ($1, $2,$3,$4,$5)', [$placa,$marca, $modelo,$kilometraje,$notas]);
        }

        public function update($id,$placa,$marca, $modelo,$kilometraje,$notas)
        {
            $this->connection->runStatement('UPDATE vehiculos SET placa=$2 marca = $3, modelo = $4, kilometraje=$5, notas=$6 WHERE id = $1', [$id,$id_cliente,$marca, $modelo,$placa,$notas]);
        }

        public function delete($id)
        {
            $this->connection->runStatement('DELETE FROM vehiculos WHERE id = $1', [$id]);
        }
    }
}
