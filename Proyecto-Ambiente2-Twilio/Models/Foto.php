<?php
namespace Models {
    class Foto
    {
        private $connection;
        public function __construct($connection)
        {
            $this->connection = $connection;
        }

        public function find($id)
        {
            return $this->connection->runQuery('SELECT * FROM fotos WHERE id = $1', [$id])[0];
        }

        public function select()
        {
            return $this->connection->runQuery('SELECT * FROM fotos ORDER BY id');
        }

        public function insert($ubicacion, $orden_id)
        {
            $this->connection->runStatement('INSERT INTO fotos(ubicacion,orden_id) VALUES ($1, $2)', [$ubicacion,$orden_id]);
        }

        public function update($id, $ubicacion,$orden_id)
        {
            $this->connection->runStatement('UPDATE fotos SET ubicacion = $2, orden_id = $3 WHERE id = $1', [$id, $ubicacion,$orden_id]);
        }

        public function delete($id)
        {
            $this->connection->runStatement('DELETE FROM fotos WHERE id = $1', [$id]);
        }
    }
}
