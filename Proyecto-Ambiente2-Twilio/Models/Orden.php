<?php
namespace Models {
    class Orden
    {
        private $connection;
        public function __construct($connection)
        {
            $this->connection = $connection;
        }

        public function find($id)
        {
            return $this->connection->runQuery('SELECT * FROM ordenes WHERE id = $1', [$id])[0];
        }
           public function find_by_cliente($cliente_id)
        {
            return $this->connection->runQuery('SELECT * FROM ordenes WHERE cliente_id = $1', [$cliente_id]);
        }


        public function select()
        {
            return $this->connection->runQuery('SELECT * FROM ordenes ORDER BY id');
        }
    public function insert($cliente_id, $vehiculo_id)
        {
            $this->connection->runStatement('INSERT INTO ordenes(cliente_id,vehiculo_id) VALUES ($1, $2)', [$cliente_id, $vehiculo_id]);
        }

        public function update($id, $cliente_id,$vehiculo_id)
        {
            $this->connection->runStatement('UPDATE ordenes SET cliente_id = $2,  vehiculo_id = $3 WHERE id = $1', [$id, $cliente_id,$vehiculo_id]);
        }

        public function delete($id)
        {
            $this->connection->runStatement('DELETE FROM ordenes WHERE id = $1', [$id]);
        }
    }
}
