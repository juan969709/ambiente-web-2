<?php

// Update the path below to your autoload.php,
// see https://getcomposer.org/doc/01-basic-usage.md
require_once '../vendor/autoload.php';
require_once '../shared/sessions.php';

use Twilio\Rest\Client;

// Find your Account Sid and Auth Token at twilio.com/console
$sid    = "AC8ef2cdda2dba644616b3a63a56fcafc8";
$token  = "b5c0e4f89efc911ba9f0479d09e4c27e";
$twilio = new Client($sid, $token);

$channels = $twilio->chat->v2->services($_SESSION['sid'])
->channels
->read();
echo "<div style='display: flex;flex-direction: row;justify-content: center;margin-top:30px''><div class='col-md-10'>
<table class='table table-striped table-bordered'>
<th>Nombre del canal</th>
<th>Unique del canal</th>
<th>Canal ID</th>
<th>Acciones</th>";

foreach ($channels as $record) {
  echo "
  <tr>";
  echo "<td>".$record->friendlyName."</td>";
  echo "<td>".$record->uniqueName."</td>";
  echo "<td>".$record->sid."	
  </td> <td>
  <form action='update-channels.php?cid=".$record->sid."' method='post'>
  <input type='text' style='width:150px' placeholder='Nombre del Canal' name='nombreCanal' required>
  <input type='text' style='width:150px' placeholder='Unique del Canal' name='uniqueName' required>
  <button class='btn btn-warning fas fa-edit mr-2' type='submit'></button>
  <a href='/channels-twilio-crud/delete-channel.php?cid=".$record->sid."' class='far fa-trash-alt btn btn-danger'></a>
  </form>

  </td>"; 
  echo "	</tr>";
}
echo "</table>
</div>
</div> 
"; 
?>




