<?php

// Update the path below to your autoload.php,
// see https://getcomposer.org/doc/01-basic-usage.md
require_once '../vendor/autoload.php';
require_once '../shared/sessions.php';

use Twilio\Rest\Client;

// Find your Account Sid and Auth Token at twilio.com/console
$sid    = "AC8ef2cdda2dba644616b3a63a56fcafc8";
$token  = "b5c0e4f89efc911ba9f0479d09e4c27e";
$twilio = new Client($sid, $token);

$channel = $twilio->chat->v2->services($_SESSION['sid'])
->channels($_SESSION['chid'])
->fetch();

print($channel->friendlyName);
