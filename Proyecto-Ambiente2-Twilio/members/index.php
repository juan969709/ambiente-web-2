<?php
$title = 'Twilio';
require_once '../shared/header.php';
require_once '../shared/sessions.php';
require_once '../shared/db.php';


if (!isset($_SESSION['usuario_id']) || empty($_SESSION['usuario_id'])) {
    return header('Location: /seguridad/login.php');
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	<h1 class="text-center">Miembros</h1>

<div class="text-center" style="display:flex;justify-content: center;flex-direction:column;">
	<form action="create-channel.php" method="post">
		<input type="text" placeholder="Nombre del Canal" name="nombreCanal" required>
    <input type="text" placeholder="Unique del Canal" name="uniqueCanal" required>
	<button class="btn btn-success mr-2" type="submit">Crear Canal</button>
</form>
<?php 
if (isset($_SESSION['chid'])) {
require_once 'list-members.php';
}else{
	print("no hay miembros");
}


 ?>
</div>


</body>
</html>