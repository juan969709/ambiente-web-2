create database ordenes;

  drop table if exist fotos;
  drop table if exist ordenes;
  drop table if exist clientes;
  drop table if exist vehiculos;

create table clientes(
  id serial primary key,
  cedula varchar not null unique,
  nombre varchar not null,
  apellidos varchar not null,
  telefono varchar not null
);

create table vehiculos(
  id serial primary key,
  cliente_id integer REFERENCES clientes(id),
  marca varchar not null,
  modelo varchar not null,
  placa varchar not null,
  notas varchar not null,
  kilometraje varchar not null
);

create table usuarios(
  id serial primary key,
  email varchar not null,
  password varchar not null
);

create table ordenes(
  id serial primary key,
  cliente_id integer not null REFERENCES clientes(id),
  vehiculo_id integer not null REFERENCES vehiculos(id)
);

create table fotos(
  id serial primary key,
  ubicacion varchar not null,
  orden_id integer not null REFERENCES ordenes(id)
);

INSERT INTO usuarios(email, password) VALUES ('juan969709@gmail.com', md5('1234'));

select * from usuarios;
