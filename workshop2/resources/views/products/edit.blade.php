@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Editar Producto</div>

                <div class="card-body">
                    @if (!empty($product))
                    <form method="post" action="/products/update/{{ $product->id }}" enctype="multipart/form-data">
                          @csrf
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label>Sku</label>
                                    <input type="text" name="sku" class="form-control input-sm" value="{{ $product->sku }}">
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label>Nombre</label>
                                    <input type="text" name="name" class="form-control input-sm" value="{{ $product->name }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label>Descripcion</label>
                                    <input name="description" class="form-control input-sm" value="{{ $product->description }}">
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label>Imagen</label>
                                    <input type="file" name="image" class="form-control input-sm" value="{{ $product->image }}">
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label>Stock</label>
                                    <input type="number" name="stock" class="form-control input-sm" value="{{ $product->stock }}">
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label>Price</label>
                                    <input type="number" name="price" class="form-control input-sm" value="{{ $product->price }}">
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <input type="submit"  value="Guardar" class="btn btn-success btn-block">
                                <a href="/products" class="btn btn-info btn-block" >Atrás</a>
                            </div>  
                        </div>
                    </form>
                    @endif
                </div>
                @if(Session::has('message'))
                <div class="alert alert-{{ Session::get('class') }}">{{ Session::get('message')}}</div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
