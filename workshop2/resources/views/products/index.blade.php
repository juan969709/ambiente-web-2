@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">Productos</div>
        <div class="card-body">
          @csrf
          <div class="text-right">
              <a href="/products/create" class="btn btn-info" >Añadir producto</a>
          </div><br>
          <div class="table-container">
            <table id="mytable" class="table table-bordred table-striped">
             <thead>
               <th>SKU</th>
               <th>Nombre</th>
               <th>Descripcion</th>
               <th>Imagen</th>
               <th>Stock</th>
               <th>Precio</th>
               <th>Opciones</th>
             </thead>
             <tbody>
              @if($products->count())  
              @foreach($products as $product)  
              <tr> 
                <td>{{ $product->sku }}</td>
                <td>{{ $product->name }}</td>
                <td>{{ $product->description }}</td>
                <td><img src="images/{{ $product->image }}"  alt="" style="height:50px;width:70px;"></td>
                <td>{{ $product->stock }}</td>
                <td>{{ $product->price }}</td>
                <td>
                  <a href="/products/edit/{{ $product->id }}"><span class="label label-success">Editar</span></a>
                  <a href="{{ url('/products/destroy',$product->id) }}" >Eliminar</a>
                 </td>
               </tr>
               @endforeach 
               @else
               <tr>
                <td colspan="10" class="text-center">No hay registro !!</td>
              </tr>
              @endif

            </tbody>

          </table>

        </div>
      </div>
    </div>
  </div>
</div>
@endsection