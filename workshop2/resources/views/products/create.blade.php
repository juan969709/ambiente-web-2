@extends('layouts.app')

@section('content')
@csrf
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Agregar nuevo producto</div>

                <div class="card-body">

                    <form method="post" action="store" enctype="multipart/form-data">
                          @csrf
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <input type="text" name="sku" class="form-control input-sm" placeholder="Sku">
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control input-sm" placeholder="Nombre producto">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <input type="text" name="description" class="form-control input-sm" placeholder="Descripcion">
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <input type="file" name="image" class="form-control input-sm" placeholder="Imagen">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                           
                            <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <input type="number" name="price" class="form-control input-sm" placeholder="Precio">
                                </div>
                            </div>
                                      <div class="col-xs-6 col-sm-6 col-md-6">
                                <div class="form-group">
                                    <input type="number" name="stock" class="form-control input-sm" placeholder="Stock">
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <input type="submit"  value="Guardar" class="btn btn-success btn-block">
                                <a href="{{ route('products.index') }}" class="btn btn-info btn-block" >Atrás</a>
                            </div>  
                        </div>
                    </form>
                </div>
                @if(Session::has('message'))
                <div class="alert alert-{{ Session::get('class') }}">{{ Session::get('message')}}</div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
