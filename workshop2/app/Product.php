<?php

namespace workshop2;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
       /**
     * Variables de Productos.
     *
     * @var array
     */
    
    protected $fillable = ['sku','name','description','image', 'stock','price'];
}
