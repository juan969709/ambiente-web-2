<?php

namespace workshop2\Http\Controllers;

use Illuminate\Http\Request;
use workshop2\Product;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Session;



class productController extends Controller
{
      /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $products = Product::orderBy('id','DESC')->get();
        return view('products.index')->with('products',$products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {       

if ($request->hasFile('image')) {
    $file =$request->file('image');
    $name_image =time().$file->getClientOriginalName();
    $file->move(public_path().'/images',$name_image);
}

         $product = new Product;
         $product->sku = Input::get('sku');
         $product->name = Input::get('name');
         $product->description = Input::get('description');
         $product->image = $name_image;
         $product->stock= Input::get('stock');
         $product->price = Input::get('price');
         if ($product->save()) {
            Session::flash('message', 'Guardado correctamente');
            Session::flash('class', 'success');
        }
        else{
            Session::flash('message', 'Ha ocurrido un error');
            Session::flash('class', 'danger');
        }
        return Redirect::to('products');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product=Product::find($id);
        return  view('products.show')->with('product',$product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product=Product::find($id);
       return view('products.edit')->with(compact('product'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        if ($request->hasFile('image')) {
    $file =$request->file('image');
    $name_image =time().$file->getClientOriginalName();
    $file->move(public_path().'/images',$name_image);
}

        $product = Product::find($id);
         $product->sku = Input::get('sku');
         $product->name = Input::get('name');
         $product->description = Input::get('description');
         $product->image =$name_image;
         $product->stock= Input::get('stock');
         $product->id_categorie = Input::get('id_categorie');
         $product->price = Input::get('price');
        if ($product->save()) {
            Session::flash('message', 'Actualizado correctamente');
            Session::flash('class', 'success');
        }
        else{
            Session::flash('message', 'Ha ocurrido un error');
            Session::flash('class', 'danger');
        }
        return Redirect::to('products');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::find($id)->delete();
        return redirect()->route('products.index')->with('success','Registro eliminado satisfactoriamente');
        Alert::message('this is a test message', 'info');
    }
}
